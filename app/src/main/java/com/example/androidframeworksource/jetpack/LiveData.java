package com.example.androidframeworksource.jetpack;

import java.util.Iterator;
import java.util.Map;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.GenericLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import static androidx.lifecycle.Lifecycle.State.DESTROYED;
import static androidx.lifecycle.Lifecycle.State.STARTED;

/**
 * @author lixiaoniu
 * @time 20-9-21 下午3:13
 */
public class LiveData {

    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        // 判断线程
        assertMainThread("observe");
        if (owner.getLifecycle().getCurrentState() == DESTROYED) {
            // 如果当前状态是DESTROYED，则不处理
            return;
        }
        // 将LifecycleOwner和Observer封装成LifecycleBoundObserver,是继承了LifecycleEventObserver
        androidx.lifecycle.LiveData.LifecycleBoundObserver wrapper = new androidx.lifecycle.LiveData.LifecycleBoundObserver(owner, observer);
        androidx.lifecycle.LiveData.ObserverWrapper existing = mObservers.putIfAbsent(observer, wrapper);
        // 同一个观察者不能添加到不同的生命周期拥有者
        if (existing != null && !existing.isAttachedTo(owner)) {
            throw new IllegalArgumentException("Cannot add the same observer"
                + " with different lifecycles");
        }
        if (existing != null) {
            return;
        }
        // LifecycleBoundObserver此时可以感知生命周期
        owner.getLifecycle().addObserver(wrapper);
    }


    /**
     * LifecycleBoundObserver可以感知生命周期
     */
    class LifecycleBoundObserver extends androidx.lifecycle.LiveData.ObserverWrapper implements GenericLifecycleObserver {
        @NonNull
        final LifecycleOwner mOwner;

        LifecycleBoundObserver(@NonNull LifecycleOwner owner, Observer<? super T> observer) {
            super(observer);
            mOwner = owner;
        }

        @Override
        boolean shouldBeActive() {
            return mOwner.getLifecycle().getCurrentState().isAtLeast(STARTED);
        }

        @Override
        public void onStateChanged(LifecycleOwner source, Lifecycle.Event event) {
            if (mOwner.getLifecycle().getCurrentState() == DESTROYED) {
                // 如果宿主时destoryed状态，移除所有观察者
                removeObserver(mObserver);
                return;
            }
            // 分发状态
            activeStateChanged(shouldBeActive());
        }

        @Override
        boolean isAttachedTo(LifecycleOwner owner) {
            return mOwner == owner;
        }

        @Override
        void detachObserver() {
            mOwner.getLifecycle().removeObserver(this);
        }
    }

    private abstract class ObserverWrapper {
        final Observer<? super T> mObserver;
        boolean mActive;
        int mLastVersion = START_VERSION;

        ObserverWrapper(Observer<? super T> observer) {
            mObserver = observer;
        }

        abstract boolean shouldBeActive();

        boolean isAttachedTo(LifecycleOwner owner) {
            return false;
        }

        void detachObserver() {
        }

        void activeStateChanged(boolean newActive) {
            if (newActive == mActive) {
                return;
            }
            mActive = newActive;
            boolean wasInactive = androidx.lifecycle.LiveData.this.mActiveCount == 0;
            androidx.lifecycle.LiveData.this.mActiveCount += mActive ? 1 : -1;
            // 活跃的观察者从0变为１
            if (wasInactive && mActive) {
                onActive();
            }
            if (androidx.lifecycle.LiveData.this.mActiveCount == 0 && !mActive) {
                onInactive();
            }
            if (mActive) {
                dispatchingValue(this);
            }
        }
    }

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    void dispatchingValue(@Nullable androidx.lifecycle.LiveData.ObserverWrapper initiator) {
        if (mDispatchingValue) {
            mDispatchInvalidated = true;
            return;
        }
        mDispatchingValue = true;
        do {
            mDispatchInvalidated = false;
            if (initiator != null) {
                // 传递的观察者不为空，这种情况一般是观察者生命周期改变的时候出发
                considerNotify(initiator);
                initiator = null;
            } else {
                // 遍历已注册的观察者,调用considerNotify分发事件
                for (Iterator<Map.Entry<Observer<? super T>, androidx.lifecycle.LiveData.ObserverWrapper>> iterator =
                     mObservers.iteratorWithAdditions(); iterator.hasNext(); ) {
                    considerNotify(iterator.next().getValue());
                    if (mDispatchInvalidated) {
                        break;
                    }
                }
            }
        } while (mDispatchInvalidated);
        mDispatchingValue = false;
    }

    private void considerNotify(androidx.lifecycle.LiveData.ObserverWrapper observer) {
        if (!observer.mActive) {
            // 不活跃，不分发
            return;
        }
        if (!observer.shouldBeActive()) {
            observer.activeStateChanged(false);
            return;
        }
        // 通过版本号判断是否已经分发过数据
        if (observer.mLastVersion >= mVersion) {
            return;
        }
        // 分发一次消息，版本对齐
        observer.mLastVersion = mVersion;
        //noinspection unchecked
        observer.mObserver.onChanged((T) mData);
    }

    @MainThread
    protected void setValue(T value) {
        // 判断线程
        assertMainThread("setValue");
        mVersion++;
        mData = value;
        dispatchingValue(null);
    }

}
